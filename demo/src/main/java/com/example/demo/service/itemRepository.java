package com.example.demo.service;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.example.demo.model.ItemObject;

public interface itemRepository extends MongoRepository<ItemObject,String>{
	
	
	ItemObject findByIdItem (String idItem);
	
}
