package com.example.demo.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection ="AccountModel")
public class AccountModel {
	
	
	@Id
	private String id;
	
	
	
	String googleID, planetName,coordenates;
	
	
	int metalAmmount, crystalAmmount, energyAmmount,levelMetalMine,levelCrystalMine,levelEnergyPlant,levelMetalStorage,levelRoboticsFactory,levelShipyard,levelMisileSilo,levelpaceDock,planetTemperature, planeteDiameter;


	public String getGoogleID() {
		return googleID;
	}


	public void setGoogleID(String googleID) {
		this.googleID = googleID;
	}


	public String getPlanetName() {
		return planetName;
	}


	public void setPlanetName(String planetName) {
		this.planetName = planetName;
	}


	public String getCoordenates() {
		return coordenates;
	}


	public void setCoordenates(String coordenates) {
		this.coordenates = coordenates;
	}


	public int getMetalAmmount() {
		return metalAmmount;
	}


	public void setMetalAmmount(int metalAmmount) {
		this.metalAmmount = metalAmmount;
	}


	public int getCrystalAmmount() {
		return crystalAmmount;
	}


	public void setCrystalAmmount(int crystalAmmount) {
		this.crystalAmmount = crystalAmmount;
	}


	public int getEnergyAmmount() {
		return energyAmmount;
	}


	public void setEnergyAmmount(int energyAmmount) {
		this.energyAmmount = energyAmmount;
	}


	public int getLevelMetalMine() {
		return levelMetalMine;
	}


	public void setLevelMetalMine(int levelMetalMine) {
		this.levelMetalMine = levelMetalMine;
	}


	public int getLevelCrystalMine() {
		return levelCrystalMine;
	}


	public void setLevelCrystalMine(int levelCrystalMine) {
		this.levelCrystalMine = levelCrystalMine;
	}


	public int getLevelEnergyPlant() {
		return levelEnergyPlant;
	}


	public void setLevelEnergyPlant(int levelEnergyPlant) {
		this.levelEnergyPlant = levelEnergyPlant;
	}


	public int getLevelMetalStorage() {
		return levelMetalStorage;
	}


	public void setLevelMetalStorage(int levelMetalStorage) {
		this.levelMetalStorage = levelMetalStorage;
	}


	public int getLevelRoboticsFactory() {
		return levelRoboticsFactory;
	}


	public void setLevelRoboticsFactory(int levelRoboticsFactory) {
		this.levelRoboticsFactory = levelRoboticsFactory;
	}


	public int getLevelShipyard() {
		return levelShipyard;
	}


	public void setLevelShipyard(int levelShipyard) {
		this.levelShipyard = levelShipyard;
	}


	public int getLevelMisileSilo() {
		return levelMisileSilo;
	}


	public void setLevelMisileSilo(int levelMisileSilo) {
		this.levelMisileSilo = levelMisileSilo;
	}


	public int getLevelpaceDock() {
		return levelpaceDock;
	}


	public void setLevelpaceDock(int levelpaceDock) {
		this.levelpaceDock = levelpaceDock;
	}


	public int getPlanetTemperature() {
		return planetTemperature;
	}


	public void setPlanetTemperature(int planetTemperature) {
		this.planetTemperature = planetTemperature;
	}


	public int getPlaneteDiameter() {
		return planeteDiameter;
	}


	public void setPlaneteDiameter(int planeteDiameter) {
		this.planeteDiameter = planeteDiameter;
	}


	public AccountModel() {
		super();
	}


	public AccountModel(String googleID, String planetName, String coordenates, int metalAmmount, int crystalAmmount,
			int energyAmmount, int levelMetalMine, int levelCrystalMine, int levelEnergyPlant, int levelMetalStorage,
			int levelRoboticsFactory, int levelShipyard, int levelMisileSilo, int levelpaceDock, int planetTemperature,
			int planeteDiameter) {
		super();
		this.googleID = googleID;
		this.planetName = planetName;
		this.coordenates = coordenates;
		this.metalAmmount = metalAmmount;
		this.crystalAmmount = crystalAmmount;
		this.energyAmmount = energyAmmount;
		this.levelMetalMine = levelMetalMine;
		this.levelCrystalMine = levelCrystalMine;
		this.levelEnergyPlant = levelEnergyPlant;
		this.levelMetalStorage = levelMetalStorage;
		this.levelRoboticsFactory = levelRoboticsFactory;
		this.levelShipyard = levelShipyard;
		this.levelMisileSilo = levelMisileSilo;
		this.levelpaceDock = levelpaceDock;
		this.planetTemperature = planetTemperature;
		this.planeteDiameter = planeteDiameter;
	}
	
	
	

}
